package unigenius.game.lists;

import unigenius.game.lists.tads.Move;

public class MovesList {
	//attributes
	private int movesLimit = 0;
	private int movesCounter = 0;
	private int index = 0;
	//private Move[] moves;
	private Move moves, first, last;
	
	
	public MovesList(int movesLimit, Move... moves){
		this.movesLimit = movesLimit;
		//this.moves = new Move[moves.length];
		try{
			for(int i = 0; i < moves.length; i++){
				if(this.moves == null){
					this.moves = moves[i];
					first = this.moves;
					last = this.moves;
					movesCounter++;
				}else{
					this.moves.setNext(moves[i]);
					moves[i].setPrev(this.moves);
					this.moves = moves[i];
					last = moves[i];
					movesCounter++;
				}
			}
		}catch(NullPointerException E){
			System.err.println("Error creating moves list! More info below...");
			System.err.println("Error: " + E.toString() + "\n...");
			printDebug();
			System.err.println("...\nClosing game...");
			System.exit(-1);
		}
		
		//System.out.println("Moves list created with success!");
	}
	
	/*public MovesList(int movesLimit, Move... moves){
		this.movesLimit = movesLimit;
		this.moves = new Move[moves.length];
		for(int i = 0; i < moves.length; i++){
			this.moves[i] = moves[i];
			movesCounter++;
		}
		//System.out.println("Moves list created with success!");
	}*/
	
	//debug
	private void printDebug(int index){
		System.err.println("moves counter: " + movesCounter);
		System.err.println("index: " + this.index);
		System.err.println("method index: " + index);
		System.err.println("first: " + first);
		System.err.println("next first: " + first.getNext());
		System.err.println("last: " + last);
		System.err.println("last prev: " + last.getPrev());
		System.err.println("moves: " + moves);
		System.err.println("moves next: " + moves.getNext());
		System.err.println("moves last: " + moves.getPrev());
	}
	
	private void printDebug(){
		System.err.println("moves counter: " + movesCounter);
		System.err.println("index: " + index);
		System.err.println("first: " + first);
		System.err.println("next first: " + first.getNext());
		System.err.println("last: " + last);
		System.err.println("last prev: " + last.getPrev());
		System.err.println("moves: " + moves);
		System.err.println("moves next: " + moves.getNext());
		System.err.println("moves last: " + moves.getPrev());
	}
	
	//checkers
	public boolean isEmpty(){
		if(movesCounter == 0){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isFull(){
		if(movesCounter == movesLimit){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isInLast(){
		if(index == movesCounter){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isInFirst(){
		if(index == 0){
			return true;
		}else{
			return false;
		}
	}
	
	//adders
	public void addMove(){
		if(isFull()){
			System.err.println("The Moves list is full!");
		}else{
			moves = first;
			try{
				for(int i = 1; i <= movesCounter; i++){
					if(moves.getNext() == null){
						last = new Move(moves);
						last.setPrev(moves);
						moves.setNext(last);
						movesCounter++;
						break;
					}else{
						moves = moves.getNext();
					}
				}
			}catch(NullPointerException E){
				System.err.println("Error adding move! More info below...");
				System.err.println("Error: " + E.toString() + "\n...");
				printDebug();
				System.err.println("...\nClosing game...");
				System.exit(-1);
			}
			
		}
	}
	

	public void addMove(int move){
		if(isFull()){
			System.err.println("The Moves list is full!");
		}else{
			moves = first;
			try{
				for(int i = 1; i <= movesCounter; i++){
					if(moves.getNext() == null){
						last = new Move(moves);
						last.setPrev(moves);
						moves.setNext(last);
						movesCounter++;
						break;
					}else{
						moves = moves.getNext();
					}
				}
			}catch(NullPointerException E){
				System.err.println("Error adding move! More info below...");
				System.err.println("Error: " + E.toString() + "\n...");
				printDebug();
				System.err.println("...\nClosing game...");
				System.exit(-1);
			}
			
		}
	}
	
	//getters
	public int getLimit(){
		return movesLimit;
	}
	
	public int getLength(){
		return movesCounter;
	}
	
	public int getIndex(){
		return index;
	}
	
	public int getCurrent(){
		if(isEmpty()){
			System.err.println("The Moves list is empty!");
			return -1;
		}else{
			try{
				return moves.getMove();
			}catch(NullPointerException E){
				System.err.println("Error getting current move! More info below...");
				System.err.println("Error: " + E.toString() + "\n...");
				printDebug();
				System.err.println("...\nClosing game...");
				System.exit(-1);
			}
			return -1;
		}
	}

	public int getMove(int index){
		if(isEmpty()){
			System.err.println("The Moves list is empty!");
			return -1;
		}else{
			moves = first;
			try{
				if(isInFirst()){
					return first.getMove();
				}else{
					for(int i = 1; i <= index; i++){
						if(i == index && moves != null){
							return moves.getMove();
						}else{
							moves = moves.getNext();
						}
					}
				}
			}catch(NullPointerException E){
				System.err.println("Error getting move at " + index + " position! More info below...");
				System.err.println("Error: " + E.toString() + "\n...");
				printDebug(index);
				System.err.println("...\nClosing game...");
				System.exit(-1);
			}
			return -1;
		}
	}
	
	public int getPrev(){
		if(isEmpty()){
			System.err.println("The Moves list is empty!");
			return -1;
		}else{
			try{
				if(isInLast()){
					index--;
					moves = last;
					return last.getMove();
				}else{
					index--;
					moves = moves.getPrev();
					return moves.getMove();
				}
			}catch(NullPointerException E){
				System.err.println("Error getting previous move! More info below...");
				System.err.println("Error: " + E.toString() + "\n...");
				printDebug();
				System.err.println("...\nClosing game...");
				System.exit(-1);
			}
			return -1;
		}
	}
	
	public int getNext(){
		if(isEmpty()){
			System.err.println("The Moves list is empty!");
			return -1;
		}else{
			try{
				if(isInFirst()){
					index++;
					moves = first;
					return first.getMove();
				}else{
					index++;
					moves = moves.getNext();
					return moves.getMove();
				}
			}catch(NullPointerException E){
				System.err.println("Error getting next move! More info below...");
				System.err.println("Error: " + E.toString() + "\n...");
				printDebug();
				System.err.println("...\nClosing game...");
				System.exit(-1);
			}
			return -1;
		}
	}
	
	public int getNextRandomized(){
		if(isEmpty()){
			System.err.println("The Moves list is empty!");
			return -1;
		}else{
			try{
				if(index == 0){
					index++;
					moves = first;
					moves.randomize();
					return moves.getMove();
				}else{
					index++;
					moves = moves.getNext();
					return moves.getMove();
				}
			}catch(NullPointerException E){
				System.err.println("Error getting next randomized move! More info below...");
				System.err.println("Error: " + E.toString() + "\n...");
				printDebug();
				System.err.println("...\nClosing game...");
				System.exit(-1);
			}
			return -1;
		}
	}
	
	public int getLast(){
		if(isEmpty()){
			System.err.println("The Moves list is empty!");
			return -1;
		}else{
			return last.getMove();
		}
	}
	
	//index handling
	public void fillIndex(){
		index = movesCounter;
	}
	
	public void resetIndex(){
		index = 0;
	}
}