package unigenius.game.lists;

import unigenius.game.Game;
import unigenius.game.players.CPU;
import unigenius.game.players.PlayerOne;
import unigenius.game.players.PlayerTwo;
import unigenius.game.players.Players;

public class PlayersList {
	//attributes
	//private Players[] players;
	//private int activePlayer = 0;
	private Players players, first, last;
	private boolean hasFinished = false;
	
	//constructor
	public PlayersList(){
		CPU cpuOne = new CPU();
		CPU cpuTwo = new CPU();
		PlayerOne playerOne = new PlayerOne(cpuOne);
		PlayerTwo playerTwo = new PlayerTwo(cpuTwo);
		first = cpuOne;
		last = playerTwo;
		playerOne.setNextPlayer(cpuTwo);
		playerOne.setPrevPlayer(cpuOne);
		last.setNextPlayer(first);
		last.setPrevPlayer(cpuTwo);
		first.setNextPlayer(playerOne);
		cpuTwo.setNextPlayer(last);
		cpuOne = null;
		cpuTwo = null;
		playerOne = null;
		playerTwo = null;
		players = first;
	}
	
	//index manipulation methods
	public void resetMyIndex(){
		getActivePlayer().getList().resetIndex();
	}
	
	public void resetComboIndex(){ //resets both player and CPU index
		getActivePlayer().getList().resetIndex();
		getCPU().getList().resetIndex();
	}
	
	public void fillMyIndex(){ 
		getActivePlayer().getList().fillIndex();
	}
	
	public void fillComboIndex(){ //fills both player and CPU index
		getActivePlayer().getList().fillIndex();
		getCPU().getList().fillIndex();
	}

	//adders
	public void addMove(int move){
		if(isPlayer()){
			getActivePlayer().getList().addMove(move);
		}else{
			System.err.println("Can't add personalized move to a CPU!");
		}
	}
	
	public void addMove(){
		if(isCPU()){
			getActivePlayer().getList().addMove();
		}else{
			System.err.println("Can't add randomized move to a Player!");
		}
	}
	
	//"setters"
	public void setNextPlayer(){
		getActivePlayer().setTurn(false);
		getActivePlayer().getList().resetIndex();
		players = getActivePlayer().getNextPlayer();
		getActivePlayer().setTurn(true);
	}
	
	private void setWinner(){
		getActivePlayer().finishMe();
		if(getPlayerOne().imFinished() && getPlayerTwo().imFinished()){
			hasFinished = true;	
		}
	}
	
	public void setLooser(){
		if(!isCPU()){
			if(getActivePlayer() instanceof PlayerOne){
				getPlayerTwo().finishMe();
				players = getPlayerTwo();
				hasFinished = true;
			}else if(getActivePlayer() instanceof PlayerTwo){
				getPlayerOne().finishMe();
				players = getPlayerOne();
				hasFinished = true;
			}
		}else{
			System.err.println("Can't finish CPU!");
		}	
	}
	
	//logical methods
	public boolean isDraw(){
		if(getPlayerOne().imFinished() && getPlayerTwo().imFinished()){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean hasFinished(){
		if(!isCPU()){
			if(getCPU().getList().getLength() == getActivePlayer().getList().getLimit()){
				setWinner();
				return false;
			}else{
				return hasFinished;
			}
		}else{
			System.err.println("Can't finish CPU!");
			return false;
		}
	}
	
	public boolean isCPUinLast(){
		if(isPlayer()){
			return getCPU().getList().isInLast();
		}else{
			System.err.println("Can't check!");
			//Game.getInstance().setRunning(false);
			return false;
		}
	}
	
	public boolean isCPUinFirst(){
		if(isPlayer()){
			return getCPU().getList().isInFirst();
		}else{
			System.err.println("Can't check!");
			//Game.getInstance().setRunning(false);
			return false;
		}
	}
	
	public boolean isInLast(){
		return getActivePlayer().getList().isInLast();
	}
	
	public boolean isInFirst(){
		return getActivePlayer().getList().isInFirst();
	}
	
	public boolean compareMoveMirrored(int move){
		if(!isPlayer()){
			System.err.println("Can't compare move from a CPU!");
			return false;
		}else{
			if(getCPU().getList().getPrev() == move){
				return true;
			}else{
				return false;
			}
		}
	}
	
	public boolean compareMove(int move){
		if(!isPlayer()){
			System.err.println("Can't compare move from a CPU!");
			return false;
		}else{
			if(getCPU().getList().getNext() == move){
				return true;
			}else{
				return false;
			}
		}
	}
	
	public boolean isCPU(){
		if(getActivePlayer().isCPU()){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isPlayer(){
		if(!getActivePlayer().isCPU()){
			return true;
		}else{
			return false;
		}
	}
	
	//getters
	private Players getActivePlayer(){
		return players;
	}
	
	private Players getCPU(){
		if(isCPU()){
			System.err.println("Can't get CPU from a CPU player! Returning itself...");
			return getActivePlayer();
		}else{
			return getActivePlayer().getCPU();
		}
	}
	
	public int getCPUIndex(){
		if(isCPU()){
			System.err.println("Can't get CPU Index from a CPU player!");
			return -1;
		}else{
			return getCPU().getList().getIndex();
		}
	}
	
	public int getIndex(){
		return getActivePlayer().getList().getIndex();
	}
	
	public int getLength(){
		return getActivePlayer().getList().getLength();
	}
	
	public int getCPUCurrent(){
		if(isPlayer()){
			return getCPU().getList().getCurrent();
		}else{
			System.err.println("Active player is not a CPU! Returning players current...");
			return getActivePlayer().getList().getCurrent();
		}
	}
	
	public int getPrev(){
		return getActivePlayer().getList().getPrev();
	}
	
	public int getNext(){
		return getActivePlayer().getList().getNext();
	}
	
	public int getNextRandomized(){
		if(isCPU()){
			return getActivePlayer().getList().getNextRandomized();
		}else{
			System.err.println("Can't get next randomized from a Player! Exiting game...");
			Game.getInstance().setRunning(false);
			return -1;
		}
	}
	
	public boolean getFinished(){
		return hasFinished;
	}
	
	public String getName(){
		return getActivePlayer().getName();
	}
	
	public boolean getTurn(){
		return getActivePlayer().getMyTurn();
	}
	
	public Players getCPUOne(){
		return first;
	}
	
	public Players getPlayerOne(){
		return first.getNextPlayer();
	}
	
	public Players getCPUTwo(){
		return last.getCPU();
	}
	
	public Players getPlayerTwo(){
		return last;
	}
}
