package unigenius.game.lists.tads;

import java.util.Random;

public class Move {
	//attributes
	private int move;
	private Random rand = new Random();
	private Move prev = null;
	private Move next = null;
	
	//constructors
	public Move(){
		move = rand.nextInt(4) + 1;
	}
	
	public Move(Move prev){
		this.prev = prev;
		move = rand.nextInt(4) + 1;
	}
	
	public Move(int move){
		this.move = move;
	}
	
	//getters
	public int getMove(){
		return move;
	}
	
	public Move getNext(){
		return next;
	}
	
	public Move getPrev(){
		return prev;
	}
	
	//setters
	public void setNext(Move next){
		this.next = next;
	}
	
	public void setPrev(Move prev){
		this.prev = prev;
	}
	
	public void setMove(Move move){
		this.move = move.getMove();
	}
	
	//other methods
	public void randomize(){
		move = rand.nextInt(4) + 1;
	}
}
