package unigenius.game.states;

import java.util.Timer;
import java.util.TimerTask;

public class GhostMode extends States{
	//serial UID
	private static final long serialVersionUID = -7169101294546320699L;

	//constructor
    public GhostMode(){
        id = "Ghost";
    }
    
  //logic methods
    @Override
    protected void playerMove(int move){
        timeRemaining[0] = 5;
        timeRemaining[1] = 1;
        
        if(threadTrigger){
            threadTrigger = false;
            
            if(activePlayer.compareMove(move)){
                //System.out.println("MATCH!"); //for debug
                playerScheduler(move);
            }else{
                playsound.playSound("fail.wav");
                try { //sleeping for audio synchronization
    				Thread.sleep(1000);
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
                playerScheduler(activePlayer.getCPUCurrent());
                try { //sleeping for audio synchronization with final move
    				Thread.sleep(500);
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
                activePlayer.setLooser();
                //System.out.println("MISSED!"); // for debug
            }
        }
    }
    
    @Override
    protected void CPUMove(){
        if(activePlayer.isCPU()){
            timeRemaining[0] = 5;
            timeRemaining[1] = 1;
            int timer = 500;
            for(int count = 0; count < activePlayer.getLength(); count++){
                if(threadTrigger){
                    threadTrigger = false;
                    CPUScheduler(activePlayer.getNext(), timer, activePlayer.getIndex());
                    timer += 500;
                    break;
                } 
            }
        }
    }
    
    //timer schedulers 
    private void playerScheduler(int move){
        if(timerPlayer == null){
            timerPlayer = new Timer();
            timerPlayer.schedule(new showPlayerMove(move), 400);
        }
    }
    
    private void CPUScheduler(int move, int millis, int count){
    	if(count > -1){ //avoid thread conflicts with player moves list index (need more testing)
    		if(timerCPU[count] == null){
                timerCPU[count] = new Timer();
                timerCPU[count].schedule(new showCPUMove(move, count), millis);
            }
    	}else{ // need more testing
    		System.err.println("Unknown error! Trying to fix it...");
    		try {
				Thread.sleep(400);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    		activePlayer.resetMyIndex();
        	if(timerPlayer != null){
            	timerPlayer.cancel();
                timerPlayer = null;
            }
            for(int i = 0 ; i < timerCPU.length ; i++){
            	if(timerCPU[i] != null){
            		timerCPU[i].cancel();
                	timerCPU[i] = null;
            	}
            }
            currentStage = 0;
            threadTrigger = true;
    	}
    }

    //timer classes
    private class showPlayerMove extends TimerTask{ //shows players related sequences
        //attributes
        private final int move;
        
        //constructor
        public showPlayerMove(int move){
            this.move = move;
        }
                
        //runner
        @Override
        public void run(){
        	currentStage = move;
            playsound.playSound(move + ".wav");
            try { //sleeping for audio synchronization with moves
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
            //Ticks.getInstance().wait(300); //causes high CPU usage
            if(timerPlayer != null)
            	timerPlayer.cancel();
            timerPlayer = null;
            threadTrigger = true;
            currentStage = 0;
            if(activePlayer.isCPUinLast()){
            	threadTrigger = false;
            	try { //sleeping for smoothed transitions!
					Thread.sleep(400);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
            	//Ticks.getInstance().wait(400); //causes high CPU usage
            	if(!activePlayer.hasFinished()){
            		activePlayer.addMove(move);
                	activePlayer.resetComboIndex();
                	activePlayer.setNextPlayer();
    				if(activePlayer.getCPUOne().getMyTurn()){
    					activePlayer.addMove();
    					activePlayer.getCPUTwo().getList().addMove();
    				}
    				threadTrigger = true;
            	}
            }
        }
    }
    
    private class showCPUMove extends TimerTask{ //shows CPU related sequences
        //attributes
        private final int move;
        private final int count;
        
        //constructor
        public showCPUMove(int move, int count){
            this.move = move;
            this.count = count;
        }
                
        //runner
        @Override
        public void run(){
        	//System.out.println(activePlayer.getIndex());
        	if(activePlayer.getIndex() % 2 == 0 && activePlayer.getIndex() != 0){
        		if(activePlayer.isInLast()){
        			currentStage = move;
        		}else{
        			currentStage = 0;
        		}
        	}else{
        		currentStage = move;
        	}
            playsound.playSound(move + ".wav");
            try { //sleeping for audio synchronization with moves
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
            //Ticks.getInstance().wait(300); //causes high CPU usage
            timerCPU[count].cancel();
            timerCPU[count] = null;
            currentStage = 0;
            if(activePlayer.isInLast()){
            	activePlayer.resetMyIndex();
            	activePlayer.setNextPlayer();
            	activePlayer.resetComboIndex();
            }
            threadTrigger = true;
        }
    }
}