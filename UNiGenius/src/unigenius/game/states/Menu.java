package unigenius.game.states;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;


//import unigenius.Ticks;
import unigenius.game.Game;
import unigenius.game.ImageLoader;
import unigenius.game.StatesManager;
import unigenius.game.TextLoader;

public class Menu extends States {
	//serial UID
	private static final long serialVersionUID = -6897759513291093695L;
	//fonts
    private final TextLoader fontTitle = new TextLoader("arial", Font.BOLD, 50);
    private final TextLoader fontSignature = new TextLoader("arial", Font.PLAIN, 18);
    //events
    private final MouseMotion mouseMotion = new MouseMotion();
    private final MouseEvents mouseEvent = new MouseEvents();
    //menu buttons
    private final MenuButtons menuButtons = new MenuButtons();
    //background
    private BufferedImage background;
    
    //constructor
    public Menu(){
        id = "Menu";
    }
    
    //main methods
    @Override
    public void render(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //render start\\
        g2d.drawImage(background, 0, 0, this);
        g2d.drawString(fontTitle.display("UniGENIUS", Color.black).getIterator(), 185, 120);
        g2d.drawString(fontSignature.display("Created by Bruno Magalh�es", Color.black).getIterator(), 402, 473);
        menuButtons.drawButtons(g2d);
        //render ends\\
        
        g2d.dispose();
    }

    @Override
    public void update() {
    	/*if(Ticks.getInstance().secondsUpdate()){
    		System.out.println(Ticks.getInstance().getSeconds());
    	}*/
    }

    @Override
    public void onEnter() {
        System.out.println("Entering " + id + " state!");
        Game.getInstance().getFrame().setTitle("UniGENIUS");
        ImageLoader loader = new ImageLoader();
        try {
            background = loader.loadImage("/background.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Game.getInstance().addMouseMotionListener(mouseMotion);
        Game.getInstance().addMouseListener(mouseEvent);
        Game.getInstance().requestFocus();
    }

    @Override
    public void onExit() {
        System.out.println("Exiting " + id + " state!");
        Game.getInstance().removeMouseListener(mouseEvent);
        Game.getInstance().removeMouseMotionListener(mouseMotion);
    }
    
    //buttons classe
    private class MenuButtons{
        //fonts
        private final TextLoader fontMenu = new TextLoader("arial", Font.BOLD, 15);
        //buttons
        private final Rectangle normalMode = new Rectangle(255, 200, 120, 40);
        private final Rectangle mirrorMode = new Rectangle(255, 250, 120, 40);
        private final Rectangle ghostMode = new Rectangle(255, 300, 120, 40);
        private final Rectangle madnessMode = new Rectangle(255, 350, 120, 40);
        private final Rectangle exitButton = new Rectangle(255, 400, 120, 40);
        //events
        private MouseEvent motion;
        private MouseEvent click;
        //graphics
        private Color defaultColor;
        
        //methos
        public void drawButtons(Graphics2D g2d){
            defaultColor = g2d.getColor();
            
            clickButton();
            
            drawButton(g2d, normalMode, "Classic Mode", 7);
            drawButton(g2d, mirrorMode, "Mirror Mode", 11);
            drawButton(g2d, ghostMode, "Ghost Mode", 11);
            drawButton(g2d, madnessMode, "Madness Mode", 0);
            drawButton(g2d, exitButton, "Exit Game", 18);

            g2d.draw(normalMode);
            g2d.draw(mirrorMode);
            g2d.draw(ghostMode);
            g2d.draw(madnessMode);
            g2d.draw(exitButton);
        }
        
        public void mouseClick(MouseEvent e){
            click = e;
        }
        
        public void mousePosition(MouseEvent e){
            motion = e;
        }
        
        private void clickButton(){
            if(click != null){
                if((click.getX() >= 255 && click.getY() >= 200) && (click.getX() <= 375 && click.getY() <= 240)){
                	StatesManager.getInstance().changeState(new NormalMode());
                }else if((click.getX() >= 255 && click.getY() >= 250) && (click.getX() <= 375 && click.getY() <= 290)){
                	StatesManager.getInstance().changeState(new MirrorMode());
                }else if((click.getX() >= 255 && click.getY() >= 300) && (click.getX() <= 375 && click.getY() <= 340)){
                	StatesManager.getInstance().changeState(new GhostMode());
                }else if((click.getX() >= 255 && click.getY() >= 350) && (click.getX() <= 375 && click.getY() <= 390)){
                	StatesManager.getInstance().changeState(new MadnessMode());
                }else if((click.getX() >= 255 && click.getY() >= 400) && (click.getX() <= 375 && click.getY() <= 440)){
                	Game.getInstance().setRunning(false);
                }
            }
        }
        
        private void drawButton(Graphics2D g2d, Rectangle rect, String buttonName, int textAdjust){
            int x = (int)rect.getX();
            int y = (int)rect.getY();
            int width = (int)rect.getWidth();
            int height = (int)rect.getHeight();
            
            if(motion != null){
                if((motion.getX() >= x && motion.getY() >= y) && (motion.getX() <= (x + width) && motion.getY() <= (y + height))){
                    g2d.setColor(Color.white);
                    g2d.fillRect(x, y, width, height);
                    g2d.drawString(fontMenu.display(buttonName, Color.black).getIterator(), rect.x + (7 + textAdjust), rect.y + 25);
                    g2d.setColor(defaultColor);
                }else{
                    g2d.setColor(Color.black);
                    g2d.fillRect(x, y, width, height);
                    g2d.drawString(fontMenu.display(buttonName, Color.white).getIterator(), rect.x + (7 + textAdjust), rect.y + 25);
                    g2d.setColor(defaultColor);
                }
            }else{
                g2d.setColor(Color.black);
                g2d.fillRect(x, y, width, height);
                g2d.drawString(fontMenu.display(buttonName, Color.white).getIterator(), rect.x + (7 + textAdjust), rect.y + 25);
                g2d.setColor(defaultColor);
            }
        }
    }
    
    //events classes
    private class MouseEvents implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent me) {
            menuButtons.mouseClick(me);
        }

        @Override
        public void mousePressed(MouseEvent me) {
        }

        @Override
        public void mouseReleased(MouseEvent me) {
        }

        @Override
        public void mouseEntered(MouseEvent me) {
        }

        @Override
        public void mouseExited(MouseEvent me) {
        }
        
    }
    
    private class MouseMotion implements MouseMotionListener{

        @Override
        public void mouseDragged(MouseEvent me) {
            
        }

        @Override
        public void mouseMoved(MouseEvent me) {
            menuButtons.mousePosition(me);
        }
        
    }
    
}
