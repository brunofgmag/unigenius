package unigenius.game.states;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener; 
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Timer;

import javax.swing.JOptionPane;

import unigenius.Ticks;
import unigenius.game.Game;
import unigenius.game.ImageLoader;
import unigenius.game.PlaySound;
import unigenius.game.TextLoader;
import unigenius.game.StatesManager;
import unigenius.game.lists.PlayersList;

public abstract class States extends Canvas {
	//serial UID
	private static final long serialVersionUID = 6358620639208236208L;
    //attributes
    protected String id;
    protected final BufferedImage[] genius = new BufferedImage[6];
    protected int currentStage = 0;
    protected final PlaySound playsound = new PlaySound();
    protected final TextLoader fontOne = new TextLoader("arial", Font.BOLD, 16);
    protected Timer countDown;
    protected final int[] timeRemaining = new int[2];
    //timers
    protected Timer timerPlayer;
    protected final Timer[] timerCPU = new Timer[31];
    //controllers
    protected boolean gameOver = false; //has the game finished?
    protected boolean threadTrigger = true; //make sure that a new move is displayed only when the last one is finished
    //players
    protected PlayersList activePlayer;
    //protected int activePlayer = 0;
    //protected final Players[] player = new Players[4];
    //events
    protected final MouseEvents mousevents = new MouseEvents();
    protected final KeyboardEvents keyboardEvents = new KeyboardEvents();
    
    public void render(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //render starts here\\
        
        g2d.drawImage(genius[currentStage], 0, 0, this);
        g2d.drawString(fontOne.display("" + activePlayer.getPlayerOne().getName() + "", Color.white).getIterator(), 5, 15);
        if(activePlayer.getPlayerOne().imFinished()){
        	g2d.drawString(fontOne.display("Finished!", Color.red).getIterator(), 5, 35);
        }else{
        	g2d.drawString(fontOne.display("Round: " + (activePlayer.getPlayerOne().getList().getLength()) + "", Color.white).getIterator(), 5, 35);
        }
        g2d.drawString(fontOne.display("" + activePlayer.getPlayerTwo().getName() + "", Color.white).getIterator(), 500, 15);
        if(activePlayer.getPlayerTwo().imFinished()){
        	g2d.drawString(fontOne.display("Finished!", Color.red).getIterator(), 500, 35);
        }else{
        	g2d.drawString(fontOne.display("Round: " + (activePlayer.getPlayerTwo().getList().getLength()) + "", Color.white).getIterator(), 500, 35);
        }
        g2d.drawString(fontOne.display("" + activePlayer.getName() + " is playing...", Color.white).getIterator(), 250, 53);
        
        if(activePlayer.isPlayer()){
            if(timeRemaining[0] == 0){
                g2d.drawString(fontOne.display("Remaining Time: " + timeRemaining[0] + "." + timeRemaining[1] + "", Color.red).getIterator(), 260, 33);
            }else{
                g2d.drawString(fontOne.display("Remaining Time: " + timeRemaining[0] + "." + timeRemaining[1] + "", Color.white).getIterator(), 260, 33);
            }         
        }

        //render ends here\\
        g2d.dispose();
    }
    
    public void update(){
        if(activePlayer.isPlayer()){
        	if(Ticks.getInstance().millisUpdate()){
        		if(!gameOver){
                    timeRemaining[1]--;
                    if(timeRemaining[1] == 0 && timeRemaining[0] > 0){
                        timeRemaining[0]--;
                        timeRemaining[1] = 9;
                    }else if(timeRemaining[0] == 0 && timeRemaining[1] == 0){
                    	gameOver = true;
                    	timeRemaining[1] = 0;
                    	timeRemaining[1] = 0;
                    }
                }else{
                    playsound.playSound("fail.wav");
                    try { //sleeping for audio synchronization
        				Thread.sleep(1000);
        			} catch (InterruptedException e) {
        				e.printStackTrace();
        			}
                    timeRemaining[1] = 0;
                    timeRemaining[0] = 0;
                    activePlayer.setLooser();
                }
            } 
        }
        if(activePlayer.getFinished()){
            gameOver = true;
            if(timerPlayer != null){
            	timerPlayer.cancel();
                timerPlayer = null;
            }
            for(int i = 0 ; i < timerCPU.length ; i++){
            	if(timerCPU[i] != null){
            		timerCPU[i].cancel();
                	timerCPU[i] = null;
            	}
            }
            if(activePlayer.isDraw()){
            	playsound.playSound("win.wav");
            	try { //sleeping for audio synchronization with moves
    				Thread.sleep(500);
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
            	int response = JOptionPane.showConfirmDialog(null, "It's a draw!\nDo you want to play again?", "Game Over!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.NO_OPTION) {
                	Game.getInstance().setRunning(false);
                } else if (response == JOptionPane.YES_OPTION) {
                	StatesManager.getInstance().changeState(new Menu());
                } else if (response == JOptionPane.CLOSED_OPTION) {
                	Game.getInstance().setRunning(false);
                }
            }else{
            	int response = JOptionPane.showConfirmDialog(null, "" + activePlayer.getName() + " has won!\nDo you want to play again?", "Game Over!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.NO_OPTION) {
                	Game.getInstance().setRunning(false);
                } else if (response == JOptionPane.YES_OPTION) {
                	StatesManager.getInstance().changeState(new Menu());
                } else if (response == JOptionPane.CLOSED_OPTION) {
                	Game.getInstance().setRunning(false);
                }
            }
        }
        if(!gameOver){
            if(activePlayer.isCPU()){
                if(activePlayer.getTurn() && (!activePlayer.getPlayerOne().getMyTurn() && !activePlayer.getPlayerTwo().getMyTurn())){
                    //player[activePlayer].setTurn(true);
                    CPUMove();
                }
            } 
        }
    }
    
    public void onEnter(){
        System.out.println("Entering " + id + " Game state!");
        Game.getInstance().getFrame().setTitle("UniGENIUS - " + id + " Mode");
        ImageLoader loader = new ImageLoader();
        try{
            //loading images
            genius[0] = loader.loadImage("/0.png");
            genius[1] = loader.loadImage("/1.png");
            genius[2] = loader.loadImage("/2.png");
            genius[3] = loader.loadImage("/3.png");
            genius[4] = loader.loadImage("/4.png");
            genius[5] = loader.loadImage("/5.png");
        }catch(IOException e){
            e.printStackTrace();
        }
        String playerOne = JOptionPane.showInputDialog(null, "Player one, what's your name?", "What's your name?", JOptionPane.QUESTION_MESSAGE);
        String playerTwo = JOptionPane.showInputDialog(null, "Player two, what's your name?", "What's your name?", JOptionPane.QUESTION_MESSAGE);
        
        if(playerOne == null || "".equals(playerOne))
            playerOne = "Player One";
        
        if(playerTwo == null || "".equals(playerTwo))
            playerTwo = "Player Two";
        
        //adding mouse listener
        Game.getInstance().addKeyListener(keyboardEvents);
        Game.getInstance().addMouseListener(mousevents);
        
        //adding players
        activePlayer = new PlayersList();
        
        activePlayer.getPlayerOne().setName(playerOne);
        activePlayer.getPlayerTwo().setName(playerTwo);
    }
    
    public void onExit(){
        System.out.println("Exiting " + id + " Game state!");
        gameOver = true;
        threadTrigger = false;
        //timerTrigger = false;
        Game.getInstance().removeKeyListener(keyboardEvents);
        Game.getInstance().removeMouseListener(mousevents);
    }
    
    //Other methods
    protected void playerMove(int move){}
    protected void CPUMove(){}
    
    //events methods/classes
    protected class KeyboardEvents implements KeyListener{

        @Override
        public void keyTyped(KeyEvent e) {
            
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ESCAPE || e.getKeyCode() == KeyEvent.VK_PAUSE){
                int response = JOptionPane.showConfirmDialog(null, "Do you want to return to the menu?", "Game Paused!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.NO_OPTION) {
                    //return to game
                } else if (response == JOptionPane.YES_OPTION) {
                	StatesManager.getInstance().changeState(new Menu());
                } else if (response == JOptionPane.CLOSED_OPTION) {
                    //return to game
                }
            }
        }
        
    }
    protected class MouseEvents implements MouseListener{

        @Override
        public void mousePressed(MouseEvent e) {
        	//System.out.println("CLICK"); //for debug
            if(!activePlayer.isCPU() && !gameOver && threadTrigger){
            	if(activePlayer.getTurn()){
            		if((e.getX()>= 15 && e.getX() <= 313) && (e.getY() >= 60 && e.getY() <= 260)){
                        playerMove(1);
                    }
                    if((e.getX() >= 325 && e.getX() <= 625) && (e.getY() >= 60 && e.getY() <= 260)){
                        playerMove(2);
                    }
                    if((e.getX() >= 15 && e.getX() <= 313) && (e.getY() >= 270 && e.getY() <= 470)){
                        playerMove(3);
                    }
                    if((e.getX() >= 325 && e.getX() <= 625) && (e.getY() >= 270 && e.getY() <= 470)){
                        playerMove(4);
                    }
            	}
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {}

        @Override
        public void mouseReleased(MouseEvent e) {}

        @Override
        public void mouseEntered(MouseEvent e) {}

        @Override
        public void mouseExited(MouseEvent e) {}    
    }
    
    //getters
    public String getID(){
        return id;
    }
}
