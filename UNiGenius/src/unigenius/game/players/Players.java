package unigenius.game.players;

import unigenius.game.lists.MovesList;
import unigenius.game.lists.tads.Move;

public abstract class Players {
    //attributes
    protected final MovesList movesList = new MovesList(31, new Move());
    protected boolean myTurn = false; //it is my turn to play?
    protected String name;
    protected Players nextPlayer; //who's the next after me?
    protected Players prevPlayer; //who's my "boss"?
    protected boolean finish = false; //have i won?
    protected boolean isCPU = false; //i'm a CPU or Player?
    
    //logic methods
    public boolean isCPU(){
    	return isCPU;
    }
    
    //setters
    public void setTurn(boolean bool){
        myTurn = bool;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setNextPlayer(Players nextPlayer){
    	this.nextPlayer = nextPlayer;
    }
    
    public void setPrevPlayer(Players nextPlayer){
    	this.prevPlayer = nextPlayer;
    }
    
    public void finishMe(){ //i am the winner!
        finish = true;
    }

    
    //getters
    public MovesList getList(){
    	return movesList;
    }
    
    public boolean imFinished(){
        return finish;
    }
    
    public boolean getMyTurn(){
        return myTurn;
    }
    
    public String getName(){
        return name;
    }
    
    public Players getNextPlayer(){
        return nextPlayer;
    }
    
    public Players getCPU(){
        return prevPlayer;
    }
}
