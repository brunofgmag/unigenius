package unigenius.game;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageLoader {
    //attributes
    private BufferedImage image;
    
    //methods
    public BufferedImage loadImage(String path) throws IOException{
        this.image = ImageIO.read(getClass().getResource(path));
        System.out.println("image loaded with success: " + path);
        return this.image;
    }
}
