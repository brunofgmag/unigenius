package unigenius.game;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import unigenius.game.states.*;

public class Game extends Canvas{
	//serial UID
	private static final long serialVersionUID = 7634906712873574637L;
	//attributes
    private String label;
    private boolean running = false;
    private JFrame frame;
    //buffer attributes
    private Graphics g;
    //singleton attributes
    private static Game instance;
    //control variables
    private boolean hasInitialized = false;

    //singleton method
    public static Game getInstance(){
        if(instance == null)
            instance = new Game();
        return instance;
    }
    
    //constructor
    private Game(){}
    
    //initializer
    public void init(String label){
        this.label = label;
        windowConfiguration();
        StatesManager.getInstance().addState(new Menu());
        hasInitialized = true;
    }
    
    //window configuration method
    private void windowConfiguration(){
        //set window resolution
        setPreferredSize(new Dimension(640, 480));
        setMaximumSize(new Dimension(640, 480));
        setMinimumSize(new Dimension(640, 480));
        
        //JFrame configuration
        frame = new JFrame(label);
        frame.add(this);
        frame.setBackground(Color.black);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        ImageLoader loader = new ImageLoader();
        try {
			Game.getInstance().getFrame().setIconImage(loader.loadImage("/icon.png"));
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        System.out.println("Window '" + label + "' has been created with success!");
    }
    
    public void render(){
        if(!hasInitialized){
            JOptionPane.showMessageDialog(frame, "Can't render! The game is not initialized!", "Error!", JOptionPane.ERROR_MESSAGE);
            System.err.println("Can't render! The game is not initialized!");
            return;
        }
        
        BufferStrategy bs = getBufferStrategy();
        
        if(bs == null){
            //triple buffer (reduce to double (2) if flickering)
            createBufferStrategy(3);
            return;
        }
        
        g = bs.getDrawGraphics();
        super.paint(g);
        
        //render starts here\\
        
        try{
        	StatesManager.getInstance().render(g);
        }catch(NullPointerException e){
            e.printStackTrace();
            System.exit(2);
        }
        
        //render ends here\\
        
        bs.show();
    }
    
    public void update(){
        if(!hasInitialized){
            JOptionPane.showMessageDialog(frame, "Can't update! The game is not initialized!", "Error!", JOptionPane.ERROR_MESSAGE);
            System.err.println("Can't update! The game is not initialized!");
            return;
        }
        
        //update starts here\\
        
        try{
        	StatesManager.getInstance().update();
        }catch(NullPointerException e){
            e.printStackTrace();
            System.exit(2);
        }
        
        //update ends here\\
    }
    
    public void clean(){
        if(!hasInitialized){
            JOptionPane.showMessageDialog(frame, "Can't exit! The game is not initialized!", "Error!", JOptionPane.ERROR_MESSAGE);
            System.err.println("Can't exit! The game is not initialized!");
            return;
        }
        StatesManager.getInstance().clean();
        frame.dispose();
    }
    
    //getters
    public JFrame getFrame() throws NullPointerException{
        if(frame == null){
            System.err.println("Error! Cannot get frame with game not initialized!");
            return null;
        }
            return frame;
    }
    
    public boolean getRunning(){
        return running;
    }
    
    //setters
    public void setRunning(boolean running){
        if(!hasInitialized){
            JOptionPane.showMessageDialog(frame, "Cannot run game if it isn't initialized!!", "Error!", JOptionPane.ERROR_MESSAGE);
            System.err.println("Error! Cannot run game if it isn't initialized!!");
            return;
        }
        this.running = running;
    }
}
