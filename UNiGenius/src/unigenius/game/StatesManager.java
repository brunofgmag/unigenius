package unigenius.game;

import java.awt.Graphics;

//import java.util.ArrayDeque;
import unigenius.game.states.States;

public class StatesManager {
    //attributes
    //private ArrayDeque<States> gameStates = new ArrayDeque<>();
    private States currentState = null;
    //singleton attribute
    private static StatesManager instance; 
    
    private StatesManager(){}
    
    //singleton method
    public static StatesManager getInstance(){
        if(instance == null)
            instance = new StatesManager();
        return instance;
    }
    
    public void addState(States newState){
        try{
        	currentState = newState;
        	currentState.onEnter();
            //gameStates.push(newState);
            //gameStates.getFirst().onEnter();
        }catch(NullPointerException e){
            e.printStackTrace();
            System.exit(2);
        }
    }
    
    /*public void popState(States newState){
        try{
            if(!gameStates.isEmpty()){
                gameStates.getFirst().onExit();
                gameStates.pop();
            }
        }catch(NullPointerException e){
            e.printStackTrace();
        }
    }*/

    public void changeState(States newState){
        try{
            if(currentState != null){
            	currentState.onExit();
            	currentState = newState;
            	currentState.onEnter();
                //gameStates.getFirst().onExit();
                //gameStates.pop();
                //gameStates.push(newState);
                //gameStates.getFirst().onEnter();
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            System.exit(2);
        }
    }
    public void render(Graphics g){
        if(currentState != null)
            currentState.render(g);
    }
    
    public void update(){
    	if(currentState != null)
            currentState.update();
    }
    
    public void clean(){
    	if(currentState != null)
            currentState.onExit();
    }
}
