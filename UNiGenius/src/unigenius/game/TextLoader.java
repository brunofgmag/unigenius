package unigenius.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.text.AttributedString;

public class TextLoader {
    //attibutes
    private final Font font;
    private AttributedString as;
    
    //constructor
    public TextLoader(String font, int type, int size){
        this.font = new Font(font, type, size);
    }
    
    //methods
    public AttributedString display(String text, Color color){
        this.as = new AttributedString(text);
        this.as.addAttribute(TextAttribute.FONT, this.font);
        this.as.addAttribute(TextAttribute.FOREGROUND, color);
        
        return this.as;
    }
}
