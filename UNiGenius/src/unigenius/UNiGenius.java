package unigenius;

import static java.lang.Math.abs;
import unigenius.game.Game;

class UNiGenius implements Runnable {
    //main window attributes
    private static final String label = "UniGENIUS";

    //thread attributes
    private Thread thread;
    
    //constructor
    private UNiGenius(){
        Game.getInstance().init(label); //initialize the game
    }
    
    //start the thread
    private synchronized void start(){
        if(Game.getInstance().getRunning())
            return;
        Game.getInstance().setRunning(true);
        thread = new Thread(this);
        thread.start();
        System.out.println("Main thread started with success!");
    }
    
    //stop the thread (disabled)
    /*private synchronized void stop(){ //not using
        if(game.getRunning())
            return;
        try {
            System.out.println("thread stopped with success!");
            thread.join();
            System.exit(1);
        } catch (InterruptedException ex) {
            Logger.getLogger(UNiGenius.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    
    //run the game
    @Override
    public void run() {
        //fps limit variables
        long lastTime = System.nanoTime();
        final int targetFps = 60;
        final long optimalTime = 1000000000 / targetFps;
        double lastFpsTime = 0;
        //int fps = 0;
        
        //game loop
        while(Game.getInstance().getRunning()){
            //fps limit variables
            long now = System.nanoTime();
            long updateLenght = now - lastTime;
            lastTime = now;
            //double delta = updateLenght / ((double)optimalTime); //delta refers to a correction value for fps variations (disabled)
            
            //update the frame counter
            lastFpsTime += updateLenght;
            //fps++;
            
            //show the current fps on the window
            if(lastFpsTime >= 1000000000){
                /*game.getFrame().setTitle(label + " - FPS: " + fps);*/
                lastFpsTime = 0;
                //fps = 0;
            }
            //update the game
            Game.getInstance().update(); 
            
           //render the game
            Game.getInstance().render();
            
            //sleep the main thread for fps limiting
            try {
            	if(abs((lastTime - System.nanoTime() + optimalTime)/1000000) <= 5){ //VERY IMPORTANT!! make sure the thread doesn't sleep for too long (states changes)
            		Thread.sleep(abs((lastTime - System.nanoTime() + optimalTime)/1000000));
            	}else{
            		Thread.sleep(5);
            	}
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
        //stop();
        Game.getInstance().clean();
        System.out.println("Exiting game...");
    }
      
    public static void main(String[] args) {
    	Ticks.getInstance().init();
        UNiGenius unigenius = new UNiGenius();
        unigenius.start();
    }
    
}
