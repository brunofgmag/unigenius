package unigenius;

public class Ticks {
	//attributes
	private long startTime;
	//singleton attributes
    private static Ticks instance;
    //control
    private long lastMillis;
    private long lastSeconds;
	
	//constructor
	private Ticks(){
		startTime = System.currentTimeMillis();
	}
	
	public void init(){
		System.out.println("Timer initialized!");
	}
	
	//singleton method
    public static Ticks getInstance(){
        if(instance == null)
            instance = new Ticks();
        return instance;
    }
	
	//methods
	public long getSeconds(){
		long currentTime = System.currentTimeMillis();
		return (currentTime - startTime) / 1000;
	}
	
	public long getMillis(){
		long currentTime = System.currentTimeMillis();
		return (currentTime - startTime) / 100;
	}
	
	public long getRawMillis(){
		long currentTime = System.currentTimeMillis();
		return (currentTime - startTime);
	}
	
	public boolean secondsUpdate(){
		if(lastSeconds == 0){
			lastSeconds = (System.currentTimeMillis() - startTime) / 1000;
		}
		
		if((System.currentTimeMillis() - startTime) / 1000 == lastSeconds){
			return false;
		}else{
			lastSeconds = (System.currentTimeMillis() - startTime) / 1000;
			return true;
		}
	}
	
	public boolean millisUpdate(){
		if(lastMillis == 0){
			lastMillis = (System.currentTimeMillis() - startTime) / 100;
		}
		
		if((System.currentTimeMillis() - startTime) / 100 == lastMillis){
			return false;
		}else{
			lastMillis = (System.currentTimeMillis() - startTime) / 100;
			return true;
		}
	}
	
	public void wait(int milli){
		long start = System.currentTimeMillis();
    	long end = System.currentTimeMillis();
    	
    	//System.out.println("Waiting " + milli + " milliseconds...");
    	
    	while(end - start <= milli){
        	//System.out.println(start - end);
        	end = System.currentTimeMillis();
        }
	}
}
